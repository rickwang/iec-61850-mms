package org.itri.iec;

import static org.junit.Assert.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.apache.commons.collections.ListUtils;
import org.itri.iec.entity.Event;
import org.itri.iec.entity.Geo;
import org.itri.iec.entity.LoadProfile;
import org.itri.iec.entity.Record;
import org.itri.iec.entity.Report;
import org.itri.iec.util.JsonUtils;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Iec61850MmsTest {
	
	Iec61850MmsEncoder encoder = new Iec61850MmsEncoder();
	Iec61850MmsDecoder decoder = new Iec61850MmsDecoder();
	
	@Test
	public void testGeo() throws IOException {
		
		int seqNum = 1;
		LocalDateTime timeOfEntry = LocalDateTime.parse("2021-09-01T11:45:00");
				
		Geo r1 = new Geo(0, "LD0",			"N0611HE73", 25.017691f, 121.53074f, "S00T01;SX21;"); // DCU 
		Geo r2 = new Geo(1, "A021000001",	"N0611HE73", 25.017691f, 121.53074f, "S00T01;SX21;A"); // FCI 順序 1
		Geo r3 = new Geo(3, "A021000003",	"N0611HE73", 25.017691f, 121.53074f, "S00T01;SX21;B"); // FCI 順序 3
		Geo r4 = new Geo(5, "A021000005",	"N0611HE73", 25.017691f, 121.53074f, "S00T01;SX21;C"); // FCI 順序 5
		
		List<Record> records = new ArrayList<Record>();
		records.add(r1);
		records.add(r2);
		records.add(r3);
		records.add(r4);
		
		Report report = new Report();
		report.setSeqNum(seqNum);
		report.setTimeOfEntry(timeOfEntry);
		report.setRecords(records);
		
		byte[] payload = encoder.encodeGeo(report);
		log.info("PDU: {}", toString(payload));				
				
		Report decoded = decoder.decode(payload);		
		log.info("Decoded: {}", JsonUtils.toPrettyPrintJson(decoded));
		
		// verify
		
		assertEquals("LD0/LLN0$BR$geoInfoBReport01", decoded.getRptId());
		assertEquals("0000000110010000", decoded.getOptFlds());
		assertEquals(seqNum, decoded.getSeqNum());
		assertEquals(timeOfEntry, decoded.getTimeOfEntry());
		assertEquals("00001111111100001111000011110000000000000000000000000000", decoded.getInclusionBitString());
		
		List<Record> rs = decoded.getRecords();
		Geo d1 = (Geo) rs.get(0);
		Geo d2 = (Geo) rs.get(1);
		Geo d3 = (Geo) rs.get(2);
		Geo d4 = (Geo) rs.get(3);
		
		assertEquals(r1.getName(),		d1.getName());
		assertEquals(r1.getLocation(),	d1.getLocation());
		assertEquals(r1.getLatitude(),	d1.getLatitude());
		assertEquals(r1.getLongitude(),	d1.getLongitude());
		assertEquals(r1.getD(),			d1.getD());		
		
		assertEquals(r2.getName(),		d2.getName());
		assertEquals(r2.getLocation(),	d2.getLocation());
		assertEquals(r2.getLatitude(),	d2.getLatitude());
		assertEquals(r2.getLongitude(),	d2.getLongitude());
		assertEquals(r2.getD(),			d2.getD());
		
		assertEquals(r3.getName(),		d3.getName());
		assertEquals(r3.getLocation(),	d3.getLocation());
		assertEquals(r3.getLatitude(),	d3.getLatitude());
		assertEquals(r3.getLongitude(),	d3.getLongitude());
		assertEquals(r3.getD(),			d3.getD());
		
		assertEquals(r4.getName(),		d4.getName());
		assertEquals(r4.getLocation(),	d4.getLocation());
		assertEquals(r4.getLatitude(),	d4.getLatitude());
		assertEquals(r4.getLongitude(),	d4.getLongitude());
		assertEquals(r4.getD(),			d4.getD());
		
		assertEquals(
				"A3 82 02 FA A0 82 02 F6 A1 07 A0 05 1A 03 52 50 54 A0 82 02 E9 8A 1C 4C 44 30 2F 4C 4C 4E 30 24 42 52 24 67 65 6F 49 6E 66 6F 42 52 65 70 6F 72 74 30 31 84 02 01 90 86 01 01 8C 06 02 85 72 60 35 BE 84 07 0F F0 F0 F0 00 00 00 8A 1C 4C 44 30 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 6F 63 61 74 69 6F 6E 8A 1C 4C 44 30 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 61 74 69 74 75 64 65 8A 1D 4C 44 30 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 6F 6E 67 69 74 75 64 65 8A 15 4C 44 30 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 64 8A 23 41 30 32 31 30 30 30 30 30 31 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 6F 63 61 74 69 6F 6E 8A 23 41 30 32 31 30 30 30 30 30 31 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 61 74 69 74 75 64 65 8A 24 41 30 32 31 30 30 30 30 30 31 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 6F 6E 67 69 74 75 64 65 8A 1C 41 30 32 31 30 30 30 30 30 31 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 64 8A 23 41 30 32 31 30 30 30 30 30 33 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 6F 63 61 74 69 6F 6E 8A 23 41 30 32 31 30 30 30 30 30 33 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 61 74 69 74 75 64 65 8A 24 41 30 32 31 30 30 30 30 30 33 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 6F 6E 67 69 74 75 64 65 8A 1C 41 30 32 31 30 30 30 30 30 33 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 64 8A 23 41 30 32 31 30 30 30 30 30 35 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 6F 63 61 74 69 6F 6E 8A 23 41 30 32 31 30 30 30 30 30 35 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 61 74 69 74 75 64 65 8A 24 41 30 32 31 30 30 30 30 30 35 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 6C 6F 6E 67 69 74 75 64 65 8A 1C 41 30 32 31 30 30 30 30 30 35 2F 4C 50 48 44 31 24 44 43 24 50 68 79 4E 61 6D 24 64 8A 09 4E 30 36 31 31 48 45 37 33 87 04 41 C8 24 3B 87 04 42 F3 0F BD 8A 0C 53 30 30 54 30 31 3B 53 58 32 31 3B 8A 09 4E 30 36 31 31 48 45 37 33 87 04 41 C8 24 3B 87 04 42 F3 0F BD 8A 0D 53 30 30 54 30 31 3B 53 58 32 31 3B 41 8A 09 4E 30 36 31 31 48 45 37 33 87 04 41 C8 24 3B 87 04 42 F3 0F BD 8A 0D 53 30 30 54 30 31 3B 53 58 32 31 3B 42 8A 09 4E 30 36 31 31 48 45 37 33 87 04 41 C8 24 3B 87 04 42 F3 0F BD 8A 0D 53 30 30 54 30 31 3B 53 58 32 31 3B 43",
				toString(payload));
	}

	@Test
	public void testEvent() throws IOException {
		
		int seqNum = 1;
		LocalDateTime timeOfEntry = LocalDateTime.parse("2021-09-01T11:45:00");
		
		Event r1 = new Event(1, "A021000001", LocalDateTime.parse("2021-07-16T07:23:42"), 0); // FCI 順序 1
		Event r2 = new Event(3, "A021000003", LocalDateTime.parse("2021-07-16T07:23:42"), 1); // FCI 順序 3
		Event r3 = new Event(5, "A021000005", LocalDateTime.parse("2021-07-16T07:23:42"), 0); // FCI 順序 5
		
		List<Record> records = new ArrayList<Record>();
		records.add(r1);
		records.add(r2);
		records.add(r3);
		
		Report report = new Report();
		report.setSeqNum(seqNum);
		report.setTimeOfEntry(timeOfEntry);
		report.setRecords(records);	
		
		byte[] payload = encoder.encodeEvent(report);
		log.info("PDU: {}", toString(payload));				
				
		Report decoded = decoder.decode(payload);		
		log.info("Decoded: {}", JsonUtils.toPrettyPrintJson(decoded));
		
		// verify
		
		assertEquals("LD0/LLN0$BR$eventInfoBReport01", decoded.getRptId());
		assertEquals("0000000110010000", decoded.getOptFlds());
		assertEquals(seqNum, decoded.getSeqNum());
		assertEquals(timeOfEntry, decoded.getTimeOfEntry());
		assertEquals("110011001100000000000000", decoded.getInclusionBitString()); // DCU & FCI number 1, 3, 5
		
		List<Record> rs = decoded.getRecords();
		Event d1 = (Event) rs.get(0);
		Event d2 = (Event) rs.get(1);
		Event d3 = (Event) rs.get(2);
		
		assertEquals(r1.getName(), 		d1.getName());
		assertEquals(r1.getTimestamp(), d1.getTimestamp());
		assertEquals(r1.getCode(), 		d1.getCode());
		
		assertEquals(r2.getName(), 		d2.getName());
		assertEquals(r2.getTimestamp(), d2.getTimestamp());
		assertEquals(r2.getCode(), 		d2.getCode());
		
		assertEquals(r3.getName(), 		d3.getName());
		assertEquals(r3.getTimestamp(), d3.getTimestamp());
		assertEquals(r3.getCode(), 		d3.getCode());
		
		assertEquals(
				"A3 82 01 50 A0 82 01 4C A1 07 A0 05 1A 03 52 50 54 A0 82 01 3F 8A 1E 4C 44 30 2F 4C 4C 4E 30 24 42 52 24 65 76 65 6E 74 49 6E 66 6F 42 52 65 70 6F 72 74 30 31 84 02 01 90 86 01 01 8C 06 02 85 72 60 35 BE 84 03 CC C0 00 8A 26 41 30 32 31 30 30 30 30 30 31 2F 45 56 45 4E 54 5F 47 47 49 4F 31 24 53 54 24 49 6E 74 49 6E 31 24 73 74 56 61 6C 8A 22 41 30 32 31 30 30 30 30 30 31 2F 45 56 45 4E 54 5F 47 47 49 4F 31 24 53 54 24 49 6E 74 49 6E 31 24 74 8A 26 41 30 32 31 30 30 30 30 30 33 2F 45 56 45 4E 54 5F 47 47 49 4F 31 24 53 54 24 49 6E 74 49 6E 31 24 73 74 56 61 6C 8A 22 41 30 32 31 30 30 30 30 30 33 2F 45 56 45 4E 54 5F 47 47 49 4F 31 24 53 54 24 49 6E 74 49 6E 31 24 74 8A 26 41 30 32 31 30 30 30 30 30 35 2F 45 56 45 4E 54 5F 47 47 49 4F 31 24 53 54 24 49 6E 74 49 6E 31 24 73 74 56 61 6C 8A 22 41 30 32 31 30 30 30 30 30 35 2F 45 56 45 4E 54 5F 47 47 49 4F 31 24 53 54 24 49 6E 74 49 6E 31 24 74 85 01 00 91 08 60 F1 33 FE 00 00 00 1F 85 01 01 91 08 60 F1 33 FE 00 00 00 1F 85 01 00 91 08 60 F1 33 FE 00 00 00 1F",
				toString(payload));
	}

	@Test
	public void testLoadProfile() throws IOException {
	
		int seqNum = 1;
		LocalDateTime timeOfEntry = LocalDateTime.parse("2021-09-01T11:45:00");		
		
		String name = "A021000001";
		List<Integer> currents = new ArrayList<Integer>();
		for (int i = 0; i < 48; i++) {
			currents.add(i);
		}
				
		LoadProfile r1 = new LoadProfile(1, name, LocalDateTime.parse("2021-09-01T11:45:00"), currents); // FCI 順序 1
			
		List<Record> records = new ArrayList<Record>();
		records.add(r1);
		
		Report report = new Report();
		report.setSeqNum(seqNum);
		report.setTimeOfEntry(timeOfEntry);
		report.setRecords(records);
		
		byte[] payload = encoder.encodeLoadProfile(report);
		log.info("PDU: {}", toString(payload));				
				
		Report decoded = decoder.decode(payload);		
		log.info("Decoded: {}", JsonUtils.toPrettyPrintJson(decoded));
		
		// verify
		
		assertEquals("LD0/LLN0$BR$profilesInfoBReport01", decoded.getRptId());
		assertEquals("0000000110010000", decoded.getOptFlds());
		assertEquals(seqNum, decoded.getSeqNum());
		assertEquals(timeOfEntry, decoded.getTimeOfEntry());
		assertEquals("00000011", decoded.getInclusionBitString());
		
		for (Record r : decoded.getRecords()) {
			LoadProfile d1 = (LoadProfile) r;
			
			assertEquals(name + "/PROFILES_QITR1$ST$EvtCnt$hstVal", d1.getName());			
			assertEquals(r1.getTimestamp(), d1.getTimestamp());
			assertTrue(ListUtils.isEqualList(r1.getCurrents(), d1.getCurrents()));
		}
		
		assertEquals(
				"A3 82 01 37 A0 82 01 33 A1 07 A0 05 1A 03 52 50 54 A0 82 01 26 8A 21 4C 44 30 2F 4C 4C 4E 30 24 42 52 24 70 72 6F 66 69 6C 65 73 49 6E 66 6F 42 52 65 70 6F 72 74 30 31 84 02 01 90 86 01 01 8C 06 02 85 72 60 35 BE 84 01 03 8A 2A 41 30 32 31 30 30 30 30 30 31 2F 50 52 4F 46 49 4C 45 53 5F 51 49 54 52 31 24 53 54 24 45 76 74 43 6E 74 24 68 73 74 56 61 6C 8A 25 41 30 32 31 30 30 30 30 30 31 2F 50 52 4F 46 49 4C 45 53 5F 51 31 54 52 31 24 53 54 24 45 76 74 43 6E 74 24 74 A1 82 00 90 85 01 00 85 01 01 85 01 02 85 01 03 85 01 04 85 01 05 85 01 06 85 01 07 85 01 08 85 01 09 85 01 0A 85 01 0B 85 01 0C 85 01 0D 85 01 0E 85 01 0F 85 01 10 85 01 11 85 01 12 85 01 13 85 01 14 85 01 15 85 01 16 85 01 17 85 01 18 85 01 19 85 01 1A 85 01 1B 85 01 1C 85 01 1D 85 01 1E 85 01 1F 85 01 20 85 01 21 85 01 22 85 01 23 85 01 24 85 01 25 85 01 26 85 01 27 85 01 28 85 01 29 85 01 2A 85 01 2B 85 01 2C 85 01 2D 85 01 2E 85 01 2F 91 08 61 2F 67 BC 00 00 00 1F",
				toString(payload));
	}

	String toString(byte[] bytes) {
		StringJoiner sj = new StringJoiner(" ");
		for (byte b : bytes) {
			sj.add(String.format("%02X", b & 0x0FF));
		}
		
		return sj.toString();
	}
}
