package org.itri.iec.entity;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;

@Data
public class Report {
	public static final String GEO = "geo";
	public static final String EVENT = "event";
	public static final String PROFILES = "profiles";

	/**
	 * RptID
	 * 
	 * LD0/LLN0$BR$profilesInfoBReport01
	 * LD0/LLN0$BR$eventInfoBReport01
	 * LD0/LLN0$BR$geoInfoBReport01
	 */
	String rptId;
	
	/**
	 * Reported OptFlds
	 * 
	 * 固定為 0110010000
	 */	
	String optFlds = "0110010000";
	
	/**
	 * SeqNum 
	 */
	int seqNum;
	
	/**
	 * 報告時間
	 */
	LocalDateTime timeOfEntry;
	
	/**
	 * 根據 FCI 數量產生的 bitString 旗標
	 */
	String inclusionBitString;
	
	/**
	 * 欲傳送的資料內容，目前支援 Geo, Event, LoadProfile
	 */
	List<Record> records;
}
