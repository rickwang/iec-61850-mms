package org.itri.iec.entity;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoadProfile extends Record {
	
	/**
	 * 負荷紀錄曲線的最後一筆時間
	 */
	LocalDateTime timestamp;
	
	/**
	 * 總共 48 筆的電流值
	 */
	List<Integer> currents;	
	
	public LoadProfile(int number, String name, LocalDateTime timestamp, List<Integer> currents) {
		this.number = number;
		this.name = name;
		this.timestamp = timestamp;
		this.currents = currents;
	}	
}
