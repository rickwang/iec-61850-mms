package org.itri.iec.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Record {
	
	/**
	 * FCI 設備順序，從 1 開始，用於組合 inclusionBitString 使用
	 * 
	 */
	int number;
	
	/**
	 * FCI 編號
	 */
	String name;
}
