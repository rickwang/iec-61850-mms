package org.itri.iec.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Geo extends Record {

	/**
	 * 圖號座標
	 */
	String location;
	
	/**
	 * 緯度
	 */
	Float latitude;
	
	/**
	 * 經度
	 */
	Float longitude;
	
	/**
	 * 迴路別、饋線別、相別等資訊
	 */
	String d;	
	
	public Geo(int number, String name, String location, Float latitude, Float longitude, String d) {
		this.number = number;
		this.name = name;
		this.location = location;
		this.latitude = latitude;
		this.longitude = longitude;
		this.d = d;
	}
}
