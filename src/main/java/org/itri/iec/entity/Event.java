package org.itri.iec.entity;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Event extends Record {
	
	/**
	 * 事件發生時間
	 */
	LocalDateTime timestamp;
	
	/**
	 * FCI 事件碼
	 */
	Integer code;
	
	public Event(int number, String name, LocalDateTime timestamp, Integer code) {
		this.number = number;
		this.name = name;
		this.timestamp = timestamp;
		this.code = code;				
	}
}
