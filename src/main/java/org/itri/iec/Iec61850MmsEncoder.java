package org.itri.iec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.itri.iec.entity.Event;
import org.itri.iec.entity.Geo;
import org.itri.iec.entity.LoadProfile;
import org.itri.iec.entity.Record;
import org.itri.iec.entity.Report;
import org.itri.iec.mms.Frame;
import org.itri.iec.mms.Pdu;
import org.itri.iec.mms.ReportEncoder;

public class Iec61850MmsEncoder {
	
	ReportEncoder encoder = new ReportEncoder();
	
	/**
	 * 產生圖資報告的封包內容
	 * 
	 * @param report
	 * @return
	 * @throws IOException
	 */
	public byte[] encodeGeo(Report report) throws IOException {
			
		report.setRptId(newRptId(Report.GEO));
		report.setInclusionBitString("1111" + newInclusionBitString(4, 12, report.getRecords())); // the first '1111' is DCU (LD0)
		
		// 在 listOfAccessResult 裡面，命名 (dataRefs) 在前，資料 (value) 在後
		List<Frame> dataRefs = new ArrayList<>();
		List<Frame> values = new ArrayList<>();
		
		for (Record r : report.getRecords()) {
			Geo geo = (Geo) r;
			
			String name = geo.getName(); // FCI 編號
			
			dataRefs.add(new Frame(
					Pdu.DataType.visibleString,
					String.format("%s/LPHD1$DC$PhyNam$location", name)));
			
			dataRefs.add(new Frame(
					Pdu.DataType.visibleString,
					String.format("%s/LPHD1$DC$PhyNam$latitude", name)));
			
			dataRefs.add(new Frame(
					Pdu.DataType.visibleString,
					String.format("%s/LPHD1$DC$PhyNam$longitude", name)));
			
			dataRefs.add(new Frame(
					Pdu.DataType.visibleString,
					String.format("%s/LPHD1$DC$PhyNam$d", name)));
			
			values.add(new Frame(Pdu.DataType.visibleString, geo.getLocation()));
			values.add(new Frame(Pdu.DataType.floating, geo.getLatitude()));
			values.add(new Frame(Pdu.DataType.floating, geo.getLongitude()));	
			values.add(new Frame(Pdu.DataType.visibleString, geo.getD()));
		}
		
		return encoder.encode(report, dataRefs, values);
	}

	/**
	 * 產生事件報告的封包內容
	 * 
	 * @param report
	 * @return
	 * @throws IOException
	 */	
	public byte[] encodeEvent(Report report) throws IOException {
			
		report.setRptId(newRptId(Report.EVENT));
		report.setInclusionBitString(newInclusionBitString(2, 12, report.getRecords())); // 110011001100000000000000
		
		// 在 listOfAccessResult 裡面，命名 (dataRefs) 在前，資料 (value) 在後
		List<Frame> dataRefs = new ArrayList<>();
		List<Frame> values = new ArrayList<>();
		
		for (Record r : report.getRecords()) {
			Event e = (Event) r;
			
			String name = e.getName(); // FCI 編號
			
			dataRefs.add(new Frame(
					Pdu.DataType.visibleString,
					String.format("%s/EVENT_GGIO1$ST$IntIn1$stVal", name)));
			
			dataRefs.add(new Frame(
					Pdu.DataType.visibleString,
					String.format("%s/EVENT_GGIO1$ST$IntIn1$t", name)));
			
			values.add(new Frame(Pdu.DataType.integer, e.getCode()));
			values.add(new Frame(Pdu.DataType.utcTime, e.getTimestamp()));
		}
		
		return encoder.encode(report, dataRefs, values);
	}
	
	/**
	 * 產生負荷曲線紀錄的封包內容
	 * 
	 * @param report
	 * @return
	 * @throws IOException
	 */
	public byte[] encodeLoadProfile(Report report) throws IOException {
		
		report.setRptId(newRptId(Report.PROFILES));
		report.setInclusionBitString(newInclusionBitString(2, 1, report.getRecords())); // 11
						
		// 在 listOfAccessResult 裡面，命名 (dataRefs) 在前，資料 (value) 在後
		List<Frame> dataRefs = new ArrayList<>();
		List<Frame> values = new ArrayList<>();
		
		for (Record r : report.getRecords()) {
			LoadProfile lp = (LoadProfile) r;
			
			String name = lp.getName(); // FCI 編號
			
			dataRefs.add(new Frame(
					Pdu.DataType.visibleString,
					String.format("%s/PROFILES_QITR1$ST$EvtCnt$hstVal", name)));
			
			dataRefs.add(new Frame(
					Pdu.DataType.visibleString,
					String.format("%s/PROFILES_Q1TR1$ST$EvtCnt$t", 	name)));			
			
			List<Frame> currents = new ArrayList<>();
			
			for (Integer current : lp.getCurrents()) {
				currents.add(new Frame(Pdu.DataType.integer, current));
			}
			
			values.add(new Frame(Pdu.DataType.array, currents));
			
			values.add(new Frame(Pdu.DataType.utcTime, lp.getTimestamp()));
		}		
		
		return encoder.encode(report, dataRefs, values);
	}

	/**
	 * 產生 InformationReport 所需要制式的 RptID
	 * 
	 * @param type
	 * @return
	 */
	String newRptId(String type) {
		return String.format("LD0/LLN0$BR$%sInfoBReport01", type);
	}
	
	/**
	 * 
	 * @param width		- 對應每個 FCI 應產生幾個 bits 的 '1' 字串
	 * @param count		- 最多有多少個 FCI 裝置
	 * @param records	- 從 Record.number 得知 FCI 的編號順序
	 * @return
	 */
	
	String newInclusionBitString(int width, int count, List<Record> records) {
		String t = StringUtils.repeat('1', width);
		String f = StringUtils.repeat('0', width);
		
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0;i < count;i++) {
			int number = i + 1;	// from FCI number 1
			
			Optional<Record> o = records.stream()
				.filter(r -> r.getNumber() == number)
				.findAny();
			
			sb.append(o.isPresent()? t : f);
		}
		
		return sb.toString();
	}
}
