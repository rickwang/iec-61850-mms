package org.itri.iec.mms;

import lombok.Data;

/**
 * MMS 封包內每個巢狀區塊
 */

@Data
public class Frame {
	
	int type;
	byte[] bytes;
	Object value;
	
	public Frame(int type) {
		this.type = type;
	}
	
	public Frame(Pdu.DataType type, Object value) {
		this.type = type.getValue();
		this.value = value;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getValue() {
		return (T) value;
	}
}
