package org.itri.iec.mms;

import java.util.NoSuchElementException;

public class Pdu {

	public enum Type {

		confirmedRequest	(0xA0),
		confirmedResponse	(0xA1),
		confirmedError		(0xA2),
		unconfirmed			(0xA3),
		reject				(0xA4),
		cancelRequest		(0xA5),
		cancelResponse		(0xA6),
		cancelError			(0xA7),
		initiateRequest		(0xA8),
		initiateResponse	(0xA9),
		initiateError		(0xAA),
		concludeRequest		(0xAB),
		concludeResponse	(0xAC),
		concludeError		(0xAD);

		int value;

		private Type(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static Type valueOf(int value) {

			for (Type mp : values()) {
				if (mp.value == value) {
					return mp;
				}
			}

			throw new NoSuchElementException(String.format("%02X is not found", value));
		}
	}

	public enum UnconfirmedService {

		informationReport	(0xA0),
		unsolicitedStatus	(0xA1), 
		eventNotification	(0xA2),
		additionalService	(0xA3);

		private int value;

		private UnconfirmedService(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static UnconfirmedService valueOf(int value) {

			for (UnconfirmedService mp : values()) {
				if (mp.value == value) {
					return mp;
				}
			}

			throw new NoSuchElementException(String.format("%02X is not found", value));
		}
	}

	public enum VariableAccessSpecification {

		listOfVariable		(0xA0), 
		variableListName	(0xA1);

		private int value;

		private VariableAccessSpecification(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static VariableAccessSpecification valueOf(int value) {

			for (VariableAccessSpecification mp : values()) {
				if (mp.value == value) {
					return mp;
				}
			}

			throw new NoSuchElementException(String.format("%02X is not found", value));
		}
	}

	public enum ObjectName {

		vmdSpecific		(0xA0), 
		domainSpecific	(0xA1),
		aaSpecific		(0xA3);

		private int value;

		private ObjectName(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static ObjectName valueOf(int value) {

			for (ObjectName mp : values()) {
				if (mp.value == value) {
					return mp;
				}
			}

			throw new NoSuchElementException(String.format("%02X is not found", value));
		}
	}
	
	public static int listOfAccessResult = 0xA0;

	public enum AccessResult {

		failure	(0xA0),
		success	(0xA1);

		private int value;

		private AccessResult(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public enum DataType {

		array			(0xA1),
		structure		(0xA2),
		bln				(0x83),
		bitString		(0x84),
		integer			(0x85),
		unsigned		(0x86),
		floating		(0x87),
		real			(0x88),
		octetString		(0x89),
		visibleString	(0x8A),
		generalizedTime	(0x8B), 
		binaryTime		(0x8C), 
		bcd				(0x8D),
		booleanArray	(0x8E), 
		objectIdentifier(0x8F), 
		utcTime			(0x91);

		private int value;

		private DataType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static DataType valueOf(int value) {

			for (DataType mp : values()) {
				if (mp.value == value) {
					return mp;
				}
			}
			
			throw new NoSuchElementException(String.format("%02X is not found", value));
		}
	}
}
