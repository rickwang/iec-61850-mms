package org.itri.iec.mms;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 負責各種 MMS 資料結構的轉換
 *
 */
public class DataTypeCodec {	
	
	public byte[] encodeFrames(boolean needHeader, List<Frame> frames) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		for (Frame f : frames) {
			Pdu.DataType type = Pdu.DataType.valueOf(f.getType());
			Object value = f.getValue();
			
			if (type == Pdu.DataType.array) {
				List<Frame> array = (List<Frame>) value;
				baos.write(encodeFrames(true, array));
				
			} else {
				baos.write(encode(type, value));
			}
		}
		
		byte[] bytes = baos.toByteArray();
		if (needHeader == false) {
			return bytes;
		}
		
		baos.reset();
		
		write(baos, Pdu.DataType.array, bytes); 
		
		return baos.toByteArray();
	}
	
	public byte[] encode(Pdu.DataType type, Object value) throws IOException {
		
		switch (type) {
		case bln:
			return toBoolean((Boolean) value);
		case bitString:
			return toBitString((String) value);
		case integer:
			return toInteger((Integer) value);
		case unsigned:
			return toUnsigned((Integer) value);
		case floating:
			return toFloating((Float) value);
		case real: // TODO
			break;
		case octetString:
			return toOctetString((String) value);
		case visibleString:
			return toVisibleString((String) value);
		case generalizedTime: // TODO
			break;
		case binaryTime:
			return toBinaryTime((LocalDateTime) value);
		case bcd: // TODO
			break;
		case booleanArray: // TODO
			break;			
		case objectIdentifier: // TODO
			break;
		case utcTime:
			return toUtcTime((LocalDateTime) value);
			
		default:
			break;
		}
		
		throw new UnsupportedOperationException("not yet supported - " + type);
	}
	
	// ======
	
	byte[] toBoolean(Boolean value) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		writeType(baos, Pdu.DataType.bln);
		writeLength(baos, 1);
		baos.write(value? 1 : 0);
		
		return baos.toByteArray();
	}

	byte[] toBitString(String value) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		boolean hit = false;		
		long v = Long.valueOf(value, 2);	// FIXME - the data size could be over the 64 bits
		for (int i = Long.BYTES - 1; i >= 0; i--) {
			int shift = i * Byte.SIZE;
			
			long m = 0x0FFL << shift;
			int b = (int) ((v & m) >> shift);
			
			if (hit || (b > 0)) {
				hit = true;
				baos.write(b);
			}
		}
		
		byte[] bytes = baos.toByteArray();
		
		baos.reset();
		
		write(baos, Pdu.DataType.bitString, bytes);
		
		return baos.toByteArray();
	}

	byte[] toInteger(Integer value) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		baos.write(BigInteger.valueOf(value).toByteArray());
		
		byte[] bytes = baos.toByteArray();
		
		baos.reset();
		
		write(baos, Pdu.DataType.integer, bytes);
		
		return baos.toByteArray();
	}
	
	byte[] toUnsigned(Integer value) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		baos.write(BigInteger.valueOf(value).toByteArray());
		
		byte[] bytes = baos.toByteArray();
		
		baos.reset();
		
		write(baos, Pdu.DataType.unsigned, bytes);
		
		return baos.toByteArray();
	}

	byte[] toFloating(float value) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		
		dos.writeFloat(value);
		dos.flush();
		
		byte[] bytes = baos.toByteArray();
		
		baos.reset();
		
		write(dos, Pdu.DataType.floating, bytes);
		dos.flush();
		
		return baos.toByteArray();		
	}
	
	// TODO - toReal()

	byte[] toOctetString(String value) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		int s = value.length();
		for (int i = 0; i < s; i += 2) {
			int b = Integer.parseInt(value.substring(i, i + 2), 16);
			baos.write(b);
		}

		byte[] bytes = baos.toByteArray();
		
		baos.reset();
		
		write(baos, Pdu.DataType.octetString, bytes);		
		
		return baos.toByteArray();
	}
	
	byte[] toVisibleString(String value) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		byte[] bytes = value.getBytes();
		
		write(baos, Pdu.DataType.visibleString, bytes);

		return baos.toByteArray();
	}
	
	// TODO - toGeneralizedTime()

	byte[] toBinaryTime(LocalDateTime date) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		
		LocalDateTime today = date.truncatedTo(ChronoUnit.DAYS);
		LocalDateTime base = LocalDateTime.parse("1984-01-01T00:00:00");
		
		long ms = Duration.between(today, date).toMillis();
		dos.writeInt((int) ms);		
		
		long days = Duration.between(base, date).toDays();
		dos.writeShort((int) days);
		
		dos.flush();
	
		byte[] bytes = baos.toByteArray();
		
		baos.reset();
		
		write(dos, Pdu.DataType.binaryTime, bytes);
		dos.flush();
		
		return baos.toByteArray();
	}
	
	// TODO - toBcd()
	// TODO - toBooleanArray()
	// TODO - toObjectIdentifier()

	byte[] toUtcTime(LocalDateTime date) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		
		LocalDateTime base = LocalDateTime.parse("1970-01-01T00:00:00");
		
		long sec = Duration.between(base, date).toMillis() / 1_000L;
		
		dos.writeInt((int) sec);
		dos.write(new byte[] { 0x00, 0x00, 0x00, 0x1F });
		dos.flush();		
		
		byte[] bytes = baos.toByteArray();
		
		baos.reset();
		
		write(dos, Pdu.DataType.utcTime, bytes);
		dos.flush();
		
		return baos.toByteArray();
	}

	// ======
	
	public void write(OutputStream os, Pdu.DataType type, byte[] bytes) throws IOException {
		
		writeType(os, type);
		writeLength(os, bytes.length);
		os.write(bytes);
	}
	
	public void writeType(OutputStream os, Pdu.DataType type) throws IOException {
		
		os.write(type.getValue());
	}
	
	public void writeLength(OutputStream os, int len) throws IOException {
			
		if (len <= 127) {
			os.write(len);
			
		} else {
			os.write(0x82);
			os.write((len & 0x0FF00) >> 8);
			os.write(len & 0x0FF);			
		}		
	}
	
	// ======
	
	public Object decode(Frame f) throws IOException {
		
		Pdu.DataType type = Pdu.DataType.valueOf(f.getType());
		
		byte[] bytes = f.getBytes();

		switch (type) {
		case array:
			return toArray(bytes);
		case structure: // TODO
			break;
		case bln:
			return toBoolean(bytes);
		case bitString:
			return toBitString(bytes);
		case integer:
			return toInteger(bytes);
		case unsigned:
			return toUnsigned(bytes);
		case floating:
			return toFloating(bytes);
		case real: // TODO
			break;
		case octetString: // TODO
			break;
		case visibleString:
			return toVisibleString(bytes);
		case generalizedTime: // TODO
			break;
		case binaryTime:
			return toBinaryTime(bytes);
		case bcd: // TODO
			break;
		case booleanArray: // TODO
			break;
		case objectIdentifier: // TODO
			break;
		case utcTime:
			return toUtcTime(bytes);
		default:
			break;
		}

		throw new UnsupportedOperationException("not yet supported - " + type);
	}
	
	List<?> toArray(byte[] bytes) throws IOException {
		List<Object> array = new ArrayList<>();
		
		FrameReader fr = new FrameReader(bytes);
		while (fr.hasNext()) {
			Frame f = fr.next();
			Object e = decode(f);
			array.add(e);
		}
		
		return array;
	}
	
	Boolean toBoolean(byte[] bytes) {
		
		return (bytes[0] > 0);		
	}
	
	String toBitString(byte[] bytes) {
		
		long v = toLong(bytes); // FIXME - 64 bits limit
		String s = Long.toBinaryString(v);
		return StringUtils.leftPad(s, bytes.length * 8, '0');
	}
	
	Integer toInteger(byte[] bytes) {
		
		return (int) (toLong(bytes) & 0x0FFFFFFFF);
	}
	
	Integer toUnsigned(byte[] bytes) {
		
		return toInteger(bytes);	// FIXME - unsinged handling
	}

	Long toLong(byte[] bytes, int offset, int length) {
		
		long v = 0;
		
		int bound = offset + length;
		
		for (int i = offset;i < bound;i++) {
			byte b = bytes[i];
			
			v = v << 8;
			v = v | (b & 0x0FF);
		}
		
		return v;
	}
	
	Long toLong(byte[] bytes) {
		
		return toLong(bytes, 0, bytes.length);
	}
	
	Float toFloating(byte[] bytes) {
		
		int v = toInteger(bytes);
		return Float.intBitsToFloat(v);
	}
	
	// TODO - toReal()
	// TODO - toOctetString()
	
	String toVisibleString(byte[] bytes) {
		
		return new String(bytes);
	}
	
	// TODO - toGeneralizedTime()
	
	LocalDateTime toBinaryTime(byte[] bytes) {
		
		LocalDateTime base = LocalDateTime.parse("1984-01-01T00:00:00");
		
		long ms = toLong(bytes, 0, 4);
		long days = toLong(bytes, 4, bytes.length - 4);
		
		return base.plusDays(days).plusSeconds(ms / 1_000L);
	}
	
	// TODO - toBcd()
	// TODO - toBooleanArray()
	// TODO - toObjectIdentifier()
	
	LocalDateTime toUtcTime(byte[] bytes) {
				
		LocalDateTime base = LocalDateTime.parse("1970-01-01T00:00:00");
		
		long seconds = toLong(bytes, 0, 4);
		
		return base.plusSeconds(seconds);
	}	
}
