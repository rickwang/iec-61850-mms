package org.itri.iec.mms;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.itri.iec.entity.Report;

public class ReportEncoder {
	
	static byte[] VMD = new byte[] { (byte) 0x00A1, 0x0007, (byte) 0x00A0, 0x0005, 0x001A, 0x0003, 0x0052, 0x0050, 0x0054 };
	
	DataTypeCodec codec = new DataTypeCodec();
	
	/**
	 * 建立 InformationReport 的封包內容
	 * 
	 * @param report		InformationReport 的表頭，也是放在 listOfAccessResult 前面 5 個
	 * @param dataRefs		命名清單
	 * @param values		數值清單
	 * @return
	 * @throws IOException
	 */
	public byte[] encode(Report report, List<Frame> dataRefs, List<Frame> values) throws IOException {
		
		byte[] accessResults = encodeAccessResults(report, dataRefs, values); // 建立 listOfAccessReport 那層
		byte[] informationReport = wrapInformationReport(accessResults); // 包一層 variableAccessSpecificatoin & InformationReport 的外殼
		byte[] packet = wrapUnconfirmedPdu(informationReport); // 再包一層 unconfirmed-PDU 的外殼
		
		return packet;
	}
	
	byte[] encodeAccessResults(Report report, List<Frame> dataRefs, List<Frame> values) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
				
		baos.write(codec.encode(Pdu.DataType.visibleString,	report.getRptId()));
		baos.write(codec.encode(Pdu.DataType.bitString,		report.getOptFlds()));
		baos.write(codec.encode(Pdu.DataType.unsigned,		report.getSeqNum()));
		baos.write(codec.encode(Pdu.DataType.binaryTime,	report.getTimeOfEntry()));
		baos.write(codec.encode(Pdu.DataType.bitString,		report.getInclusionBitString()));
		
		baos.write(codec.encodeFrames(false, dataRefs));	// 命名先
		baos.write(codec.encodeFrames(false, values));		// 數值後
				
		byte[] bytes = baos.toByteArray();
		baos.reset();
		
		baos.write(Pdu.listOfAccessResult);
		codec.writeLength(baos, bytes.length);
		baos.write(bytes);
				
		return baos.toByteArray();
	}
	
	byte[] wrapInformationReport(byte[] accessResults) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		baos.write(Pdu.UnconfirmedService.informationReport.getValue());
		codec.writeLength(baos, VMD.length + accessResults.length);
		baos.write(VMD);
		baos.write(accessResults);
				
		return baos.toByteArray();
	}
	
	byte[] wrapUnconfirmedPdu(byte[] informationReport) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		baos.write(Pdu.Type.unconfirmed.getValue());
		codec.writeLength(baos, informationReport.length);
		baos.write(informationReport);
		
		return baos.toByteArray();
	}	
}
