package org.itri.iec.mms;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;

/**
 * 將封包內的巢狀區塊循序讀出 
 *
 */
public class FrameReader {

	final DataInputStream in;
	
	public FrameReader(InputStream in) {
		this.in = new DataInputStream(in);
	}
	
	public FrameReader(byte[] bytes) {
		this(new ByteArrayInputStream(bytes));
	}
	
	public FrameReader(Frame f) {
		this(f.getBytes());
	}
	
	public boolean hasNext() throws IOException {
		return (in.available() > 0);
	}
	
	public Frame next() throws IOException {
		if (hasNext() == false) {
			throw new NoSuchElementException();
		}
		
		int type = in.read(); // the first byte is the type of frame
		
		int len = in.read(); // the next byte is the length field
		if (len == 0x081) { // 1 byte unsigned integer
			len = in.read();
			
		} else if (len == 0x082) { // 2 bytes unsigned integer
			len = in.readShort();
		}
		
		byte[] bytes = new byte[len];
		in.readFully(bytes);
		
		Frame f = new Frame(type);
		f.setBytes(bytes);		
				
		return f;	
	}
}
