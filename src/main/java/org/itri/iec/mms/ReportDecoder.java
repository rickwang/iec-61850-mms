package org.itri.iec.mms;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.itri.iec.ProtocolException;
import org.itri.iec.entity.Event;
import org.itri.iec.entity.Geo;
import org.itri.iec.entity.LoadProfile;
import org.itri.iec.entity.Record;
import org.itri.iec.entity.Report;

public class ReportDecoder {
	
	DataTypeCodec codec = new DataTypeCodec();

	/**
	 * 將 InformationReport 的封包內容解開，主要專注在 listOfAccessResult 的內容解析
	 * 
	 * @param f	- 屬於 InformationReport 區塊的封包內容
	 * @return
	 * @throws IOException
	 */
	public Report decode(Frame f) throws IOException {		
		List<Frame> frames = toAccessResults(f); // f is includes 'variableAccessSpecification' & 'listOfAccessResult'
		
		Report report = new Report();
		report.setRptId(				frames.get(0).getValue()); 
		report.setOptFlds(				frames.get(1).getValue());
		report.setSeqNum(				frames.get(2).getValue());
		report.setTimeOfEntry( 			frames.get(3).getValue());		
		report.setInclusionBitString(	frames.get(4).getValue());
		
		String rptId = report.getRptId();		
		
		if (rptId.contains(Report.GEO)) { // rptId is 'LD0/LLN0$BR$geoInfoBReport01'
			report.setRecords(toGeoRecords(frames));
			
		} else if (rptId.contains(Report.EVENT)) { // rptId is 'LD0/LLN0$BR$eventInfoBReport01'
			report.setRecords(toEventRecords(frames));
			
		} else if (rptId.contains(Report.PROFILES)) { // rptId is 'LD0/LLN0$BR$profilesInfoBReport01'
			report.setRecords(toLoadProfileRecords(frames));
			
		} else {
			throw new ProtocolException(String.format("Unsupported RptId - %s", rptId));
		}
						
		return report;
	}
	
	List<Record> toGeoRecords(List<Frame> frames) {
		
		int start = 5;
		int shift = (frames.size() - start) / 2; // the offset between dataRef & value
		int end = start + shift;
		
		List<Record> records = new ArrayList<>();
	
		for (int i = start; i < end; i += 4) {
			String name = (String) frames.get(i).getValue();
			name = name.split("/")[0];
			
			int next = i + shift;
			
			String location	= frames.get(next).getValue();
			Float latitude	= frames.get(next + 1).getValue();
			Float longitude = frames.get(next + 2).getValue();
			String d		= frames.get(next + 3).getValue();

			int number = 0; // FIXME - from inclusionBitString, but useless now
			
			Geo geo = new Geo(number, name, location, latitude, longitude, d);
			
			records.add(geo);
		}
		
		return records;
	}
	
	List<Record> toEventRecords(List<Frame> frames) {
				
		int start = 5;
		int shift = (frames.size() - start) / 2; // the offset between dataRef & value
		int end = start + shift;
		
		List<Record> records = new ArrayList<>();
		
		for (int i = start; i < end; i += 2) {
			String name	= frames.get(i).getValue();
			name = name.split("/")[0];
			
			int next = i + shift;
			
			Integer code			= frames.get(next).getValue();
			LocalDateTime timestamp	= frames.get(next + 1).getValue();
			
			int number = 0; // FIXME - from inclusionBitString, but useless now
			
			Event e = new Event(number, name, timestamp, code);
			
			records.add(e);
		}
		
		return records;
	}
	
	List<Record> toLoadProfileRecords(List<Frame> frames) {
		
		String name = frames.get(5).getValue();
		
		// [5] is A021000003/PROFILES_QITR1$ST$EvtCnt$hstVal
		// [6] is A021000003/PROFILES_QITR1$ST$EvtCnt$t 
		// [7] is array of currents
		
		List<Integer> currents = frames.get(7).getValue();
		
		// [8] is utc-time
		
		LocalDateTime timestamp = frames.get(8).getValue(); // the last AccessResult is timestamp
		
		int number = 0; // FIXME - from inclusionBitString, but useless now
		
		LoadProfile lp = new LoadProfile(number, name, timestamp, currents);
		
		return Arrays.asList(lp);
	}
	
	// ======
	
	List<Frame> toAccessResults(Frame f) throws IOException {
		
		FrameReader reader = new FrameReader(f);
		
		// <variableAccessSpecification>
		if (reader.hasNext() == false) {
			throw new ProtocolException("Failed to read the variableAccessSpecification");
		}		
		f = reader.next();
		
		String variableAccessSpecification = toObjectName(f); // useless now
		
		// <listOfAccessResult>
		if (reader.hasNext() == false) {
			throw new ProtocolException("Failed to read the listOfAccessResult");
		}		
		f = reader.next();	
		
		List<Frame> results = new ArrayList<Frame>();
		
		reader = new FrameReader(f);
		while (reader.hasNext()) {
			Frame n = reader.next();
			
			n.setValue(codec.decode(n));
			
			results.add(n);
		}
		
		return results;
	}
	
	String toObjectName(Frame f) throws IOException {
		
		Pdu.ObjectName on = Pdu.ObjectName.valueOf(f.getType());
		
		if(on != null) {
			switch(on) {
			case vmdSpecific:				// Identifier
				return toIdentifier(f);				
			case domainSpecific:
				break;
			case aaSpecific:				// Identifier
				return toIdentifier(f);
			}
		}
		
		return null;
	}
	
	String toIdentifier(Frame f) throws IOException {
		
		FrameReader reader = new FrameReader(f);
		
		if (reader.hasNext() == false) {
			throw new ProtocolException("Failed to read the data type");
		}		
		f = reader.next();
		
		return (String) codec.decode(f);
	}
}
