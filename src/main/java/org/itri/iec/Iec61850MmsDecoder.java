package org.itri.iec;

import java.io.IOException;

import org.itri.iec.entity.Report;
import org.itri.iec.mms.Frame;
import org.itri.iec.mms.FrameReader;
import org.itri.iec.mms.ReportDecoder;
import org.itri.iec.mms.Pdu;

public class Iec61850MmsDecoder {

	ReportDecoder decoder = new ReportDecoder();
	
	public Report decode(byte[] packet) throws IOException {		
		
		FrameReader reader = new FrameReader(packet);
		Frame f = reader.next();
		
		Pdu.Type type = Pdu.Type.valueOf(f.getType());
		if (type == Pdu.Type.unconfirmed) {			
			
			reader = new FrameReader(f);
			f = reader.next();
			
			Pdu.UnconfirmedService service = Pdu.UnconfirmedService.valueOf(f.getType());
			if (service == Pdu.UnconfirmedService.informationReport) {				
				return decoder.decode(f);
			}
			
			throw new ProtocolException(String.format("Unsupported service - %02X", f.getType()));				
		}

		throw new ProtocolException(String.format("Unsupported PDU format - %02X", f.getType()));
	}
}
